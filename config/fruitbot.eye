Eye.application "fruitbot" do
  process :hubot do
    start_command "/usr/bin/env ./bin/hubot -a irc -n fruitbot"
    pid_file "/var/apps/fruitbot/shared/pids/fruitbot.pid"
    daemonize true
    env 'PYTHON' => '/usr/bin/python2',
        'HUBOT_LOG_LEVEL' => 'debug',
        'PATH' => '/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games',
        'LD_LIBRARY_PATH' => '/usr/local/lib',
        'HUBOT_IRC_NICK' => "fruitbot",
        'HUBOT_IRC_ROOMS'=>"#datafruits,#datafruitsouth",
        'HUBOT_IRC_SERVER'=>"irc.esper.net",
        'IRC_PASSWORD' => '2boobies'
    working_dir "/var/apps/fruitbot/current"
    stdall "/var/log/fruitbot.log"
  end
end
