require 'bundler/capistrano'

set :application, "fruitbot"
set :repository,  "ssh://git@bitbucket.org/mcfiredrill/fruitbot.git"

set :scm, :git # You can set :scm explicitly or Capistrano will make an intelligent guess based on known version control directory names

role :web, "creamy.at"                          # Your HTTP server, Apache/etc
role :app, "creamy.at"                          # This may be the same as your `Web` server

set :scm, :git
set :deploy_via, :remote_cache
set :deploy_to, "/var/apps/fruitbot"
set :keep_releases, 3
set :user, "deploy"
default_run_options[:pty] = true  # Must be set for the password prompt from git to work
set :npm_path,    'npm'
set :npm_options, '--production --silent'

namespace :npm do
  desc 'Runs npm install.'
  task :install, :roles => :app, :except => { :no_release => true } do
    run "cd #{latest_release} && #{npm_path} #{npm_options} install"
  end
end

namespace :hubot do
  desc "Start hubot via eye"
  task :start, :roles => :app do
    run "cd #{latest_release} && #{try_sudo} eye l config/fruitbot.eye"
    run "#{try_sudo} eye start fruitbot"
  end
  task :restart, :roles => :app do
    run "cd #{latest_release} && #{try_sudo} eye l config/fruitbot.eye"
    run "#{try_sudo} eye restart fruitbot"
  end
  task :stop, :roles => :app do
    run "#{try_sudo} eye stop fruitbot"
  end
end

after 'deploy:finalize_update', 'npm:install'
after "deploy:update", "hubot:restart"
