# Description:
#  get listener stats from datafruits server
#
#  Commands:
#   hubot stats

module.exports = (robot) ->
  robot.respond /(stats)( me)?/i, (msg) ->
    robot.http("http://radio.datafruits.fm:8000/listener_count.xsl")
      .get() (err, res, body) ->
        xml2js = require 'xml2js'
        parser = new xml2js.Parser()
        parser.parseString body,  (err, result) ->
          msg.send JSON.parse(JSON.stringify(result)).stats.listeners
