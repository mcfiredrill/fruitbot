# Description
#   get eye process monitoring info
#
#   Commands
#     hubot eye - executes sudo eye i on the server and returns the result
#
#

module.exports = (robot) ->
  robot.respond /(eye)/i, (msg) ->
    sys = require('sys')
    @exec = require('child_process').exec
    command = "eye i"
    @exec command, (error, stdout, stderr) ->
      msg.send stdout
