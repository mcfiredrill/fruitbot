# Description:
#  get current song playing on datafruits
#
#  Commands:
#   hubot what is playing on datafruits right now
#   hubot current song
#
module.exports = (robot) ->
  robot.respond /current( )?(song|dj)/i, (msg) ->
    msg.http("http://radio.datafruits.fm/currentsong")
      .get() (err, res, body) ->
        msg.send body
