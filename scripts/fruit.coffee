# Description
#  returns fruits
#
#  Commands:
#    hubot <name-of-fruit> me

module.exports = (robot) ->
  robot.respond /(fruit)( me)?/i, (msg) ->
    fruits = ["strawberry", "cherry", "pear", "peach", "shroom", "tomato", "eggplant", "grape", "melon", "watermelon", "tangerine", "lemon", "banana", "apple", "pineapple"]
    msg.send fruitMe(msg.random(fruits))

fruitMe = (name) ->
  fruits = {
    "strawberry": "🍓",
    "cherry": "🍒",
    "pear": "🍐",
    "peach": "🍑",
    "shroom": "🍄",
    "tomato": "🍅",
    "eggplant": "🍆",
    "grape": "🍇",
    "melon": "🍈",
    "watermelon": "🍉",
    "tangerine": "🍊",
    "lemon": "🍋",
    "banana": "🍌",
    "apple": "🍎",
    "pineapple": "🍍"
  }
  fruits[name]
