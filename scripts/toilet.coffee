# Description
#   returns toilet text
#
#   Commands:
#     hubot toilet <font-name> text
#     hubot toilet fonts

module.exports = (robot) ->
  robot.respond /(toilet) (\w+)(.*)/i, (msg) ->
    font = msg.match[2]
    text = msg.match[3]
    sys = require('sys')
    @exec = require('child_process').exec
    if font == "fonts"
      command = "ls /usr/local/share/figlet/"
    else
      command = "toilet -f "+font+" "+text
    @exec command, (error, stdout, stderr) ->
      msg.send stdout
      robot.logger.debug stderr
      robot.logger.debug error
